require 'securerandom'

class Channel < ActiveRecord::Base
  # Relationships
  has_many :subscriptions
  has_many :feeds, through: :subscriptions
  
  # Validation
  validates :name, presence: true
  validates :password, length: {minimum: 5}, allow_nil: true
  
  has_secure_password
  
  # Hooks
  before_validation do
    self.name = SecureRandom.hex 4 if self.name.nil?
  end
  
  def to_param
    self.name
  end
  
  def verify_password password
    if !self.authenticate password
      self.errors.add :password, 'does not match'
      false
    else
      true
    end
  end

  def add_subscriptions subscriptions
    subscriptions.map {|x| x.feed}.each {|x| Subscription.create channel: self, feed: x}
  end
  
  def news count = 90
    news = []
    
    self.subscriptions.map {|x| x.feed}.each do |feed|
      news = news + feed.news
    end
    
    return news.sort_by {|x| x[:published_at]}.last(count).reverse || []
  end

  def to_rss count = 90
    self.subscriptions.map {|x| x.feed.to_rss}
  end
end
