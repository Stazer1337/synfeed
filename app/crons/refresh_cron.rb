require 'rufus-scheduler'

Rufus::Scheduler.singleton.every '10h' do
  current_time = Time.now.to_i
  
  Feed.all.each {|feed| feed.refresh(time: current_time)}
end
