class Subscription < ActiveRecord::Base
  # Relationships
  belongs_to :channel
  belongs_to :feed

  # Validation
  validates :channel, presence: true
  validates :feed, presence: true

  # Hooks
  before_validation :check_optional
  validate :check_uniqueness

  # Constructor
  def initialize attributes = nil, options = {}
    @optional = attributes.delete(:optional) if attributes.kind_of? Hash
    super attributes, options
  end
  
  # Checks @optional
  private def check_optional
    if !@optional.nil?
      @optional[:class] = @optional[:class].to_s.constantize if !@optional[:class].kind_of? Class
      self.feed = @optional[:class].pick @optional[:attributes]
    end
  end
  
  # Checks for multiple existing subscriptions
  private def check_uniqueness
    if !self.feed.nil? && Subscription.where(channel: self.channel, feed: self.feed).size > 0
      self.errors.add(:feed, 'already subscribed') 
    end
  end
end
