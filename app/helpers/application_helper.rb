module ApplicationHelper
  private def require_channel
    @channel = Channel.find_by name: require_id(:channel_id)
    redirect_to root_path if @channel.nil?
  end

  private def require_feed
    @feed = Feed.find_by name: require_id(:feed_id)
    redirect_to root_path if @feed.nil?
  end
  
  private def require_subscription
    if params.include? :feed_id
      feed = Feed.find_by name: params[:feed_id]
      @subscription = Subscription.find_by feed_id: feed.id, channel_id: @channel.id
    else
      @subscription = Subscription.find_by id: (params[:subscription_id].nil? ? params[:id] : params[:subscription_id]), channel_id: @channel.id
      redirect_to root_path if @subscription.nil?
    end
  end
  
  private def require_id optional
    return params[optional.to_sym].nil? ? params[:id] : params[optional.to_sym]
  end
  
  private def check_channels_cookie
    session[:channels] = Array.new if session[:channels].nil?
  end

  private def require_unlocked_channel
    require_channel
    redirect_to channel_path @channel if channel_locked? @channel
  end
  
  private def channel_locked? channel
    check_channels_cookie
    !session[:channels].include? channel.name
  end

  private def channel_unlocked? channel
    check_channels_cookie
    !channel_locked? channel
  end
  
  private def unlock_channel channel
    check_channels_cookie
    session[:channels].push channel.name if channel_locked? channel
  end

  private def lock_channel channel
    check_channels_cookie
    session[:channels].delete channel.name
  end
end
