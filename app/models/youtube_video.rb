class YoutubeVideo < ActiveRecord::Base
  # Relationships
  belongs_to :feed

  # Validation
  validates :feed, presence: true
  validates :youtube_id, presence: true, uniqueness: {case_sensitive: false}

  # Puts attributes to the video with given video id
  def self.put attributes = {}
    video = YoutubeVideo.find_by(youtube_id: attributes[:youtube_id])

    if !video
      [:created, YoutubeVideo.create(attributes)]
    else
      video.name = attributes[:name]
      video.link = attributes[:link]
      video.thumbnail = attributes[:thumbnail]
      video.views = attributes[:views]
      video.length = attributes[:length]
      
      video.save

      [:updated, video]
    end
  end

  # Converts youtube video data to atom
  def to_atom
    Atom::Entry.from_attributes id: self.youtube_id, title: self.name, updated: self.updated_at,
                                link: "https://www.youtube.com/watch?v=#{self.youtube_id}", published: self.uploaded_at,
                                optional: {thumbnail: self.thumbnail}
  end
end
