class SubscriptionsController < ApplicationController
  before_action :require_unlocked_channel
  before_action :require_subscription, only: [:update, :destroy]
  
  def new
  end
  
  def create
    subscription = Subscription.new channel: @channel, optional: {class: YoutubeUser, attributes: {primary_name: params[:subscription][:name]}}
    
    if subscription.save
      redirect_to channel_path(@channel)
    else
      @errors = subscription.errors
      @errors.messages[:name] = @errors.messages[:feed]
      render 'new'
    end
  end

  def destroy
    @subscription.destroy!
    redirect_to channel_path(@channel)
  end

  def update
    @subscription.feed.refresh!
    redirect_to channel_path(@channel)
  end
end
