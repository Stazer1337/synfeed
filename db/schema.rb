# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170831120424) do

  create_table "channels", force: :cascade do |t|
    t.string   "name",            null: false
    t.string   "password_digest", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "channels", ["name"], name: "index_channels_on_name"

  create_table "feeds", force: :cascade do |t|
    t.string   "type",                      null: false
    t.integer  "refresh_rate",  default: 0, null: false
    t.string   "miscellaneous"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "feeds", ["type"], name: "index_feeds_on_type"

  create_table "subscriptions", force: :cascade do |t|
    t.integer  "channel_id", null: false
    t.integer  "feed_id",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "subscriptions", ["channel_id"], name: "index_subscriptions_on_channel_id"
  add_index "subscriptions", ["feed_id"], name: "index_subscriptions_on_feed_id"

  create_table "youtube_videos", force: :cascade do |t|
    t.integer  "feed_id",     null: false
    t.string   "youtube_id",  null: false
    t.string   "name"
    t.string   "link"
    t.string   "thumbnail"
    t.integer  "length"
    t.integer  "views"
    t.datetime "uploaded_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "youtube_videos", ["feed_id"], name: "index_youtube_videos_on_feed_id"
  add_index "youtube_videos", ["youtube_id"], name: "index_youtube_videos_on_youtube_id"

end
