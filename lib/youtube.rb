require 'uri'

module Youtube
  # Returns information about a user
  def self.user_info name
    Browser.client.goto "https://www.youtube.com/user/#{name.downcase}"
    
    document = Nokogiri::HTML(Browser.client.html)
    
    {name: URI(Browser.client.url).path.split('/').last,
     display_name: document.css('#channel-title').children.first.to_s,
     subscriptions: document.css('#subscriber-count').children.first.to_s.sub(/\./, '').to_i}
  end

  # Returns a list of the latest videos 
  def self.latest_videos name, browser = nil
    Browser.client.goto "https://www.youtube.com/user/#{name.downcase}/videos"

    document = Nokogiri::HTML(Browser.client.html)
    videos = []

    document.css('ytd-grid-video-renderer #dismissable').each do |item|
      metadata = item.css '#metadata-line'
      video_title = item.at_css('#video-title')
      thumbnail = item.at_css('img').attributes['src'].to_s

      next if thumbnail.empty? # TODO: Scrolling?
        
      videos.push({youtube_id: video_title.attributes['href'].to_s.sub(/\/watch\?v\=/, ''),
                   name: video_title.children.first.to_s,
                   views: Youtube.translate_views(metadata.children[0].children.first.to_s),
                   uploaded_at: Youtube.translate_uploaded_at(metadata.children[1].children.first.to_s),
                   length: Youtube.translate_length(0),
                   thumbnail: thumbnail})
    end
    
    videos
  end

  # Translates view string into a number
  def self.translate_views string
    0
  end
  
  # Translates length string into a number
  def self.translate_length string
    0
  end

  # Translates uploaded_at string into a timestamp
  def self.translate_uploaded_at string
    Chronic.parse(Youtube.de_to_en_timestamp(string))
  end   
  
  # Translates the given de timestamp to an en timestamp
  def self.de_to_en_timestamp string
    {'ago': '',
     'vor ': '',

     'Jahre': 'years',
     'Jahr': 'year',
     
     'Monate': 'months',
     'Monat': 'month',
     
     'Wochen': 'weeks',
     'Woche': 'week',
     
     'Tagen': 'days',
     'Tag': 'day',
     
     'Stunden': 'hours',
     'Stunde': 'hour',
     
     'Minuten': 'minutes',
     'Minute': 'minute',
     
     'Sekunden': 'seconds',
     'Sekunde': 'second'}.each do |key, value|
      string.sub!(/#{key}/i, value)
    end
    
    return string + ' ago'
  end
end
