require 'singleton'

class Browser
  include Singleton

  def initialize
      @headless = Headless.new
      @headless.start
      
      @client = Watir::Browser.new :chrome, switches: ['--window-size=1920,1080']
  end

  attr_reader :client
  
  def self.client
    Browser.instance.client
  end
end
