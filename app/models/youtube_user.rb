class YoutubeUser < Feed
  # Validation
  validates :primary_name, presence: true, uniqueness: {case_sensitive: false}
  validates :secondary_name, presence: true
  
  # Relationships
  has_many :youtube_videos, foreign_key: 'feed_id', class_name: 'YoutubeVideo'
  
  # Hooks
  before_validation :handle_name, on: :create
  
  # Returns param
  def to_param
    self.primary_name
  end

  # Search youtube user by name(case insensitive)
  def self.find_by_primary_name primary_name
    YoutubeUser.where(YoutubeUser.arel_table[:primary_name].matches(primary_name)).first
  end

  # Picks(selects or creates) a youtube user
  def self.pick options = {}
    YoutubeUser.find_by_primary_name(options[:primary_name]) || YoutubeUser.create(primary_name: options[:primary_name])
  end
  
  # Refreshs feed content, ignoring the refresh rate
  def refresh! options = {}
    data = Youtube.latest_videos(self.primary_name, options.delete(:browser) || Watir::Browser.new(:phantomjs))
    
    created = []
    updated = []
    
    data.each do |entry|
      status, video = YoutubeVideo.put(entry.merge(feed: self))
      ((status == :created) ? created : updated).push video
    end

    self.refresh_rate = YoutubeUser.calculate_refresh_rate data.map {|x| x[:uploaded_at].to_i}
    self.save

    {created: created, updated: updated}
  end
  
  # Calculates the upload rate of the given upload timestamps
  def self.calculate_refresh_rate upload_timestamps
    return 0 if upload_timestamps.empty?
    
    b = 0
    
    upload_timestamps.take(upload_timestamps.size - 1).each_index do |i|
      b += upload_timestamps[i + 1] - upload_timestamps[i]
    end

    return b / b.size
  end

  # Returns youtube user as atom feed
  def to_atom
    Atom::Feed.from_attributes id: self.primary_name, title: self.secondary_name, updated: self.updated_at
  end

  # Set name as shown on the official channel site
  private def handle_name
    if !self.primary_name.nil?
      self.primary_name = self.primary_name.downcase
      self.secondary_name = Youtube.user_info(self.primary_name)[:display_name]
    end
  end
end
