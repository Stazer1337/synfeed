class CreateYoutubeVideos < ActiveRecord::Migration
  def change
    create_table :youtube_videos do |t|
      t.references :feed, null: false, index: true
      
      t.string :youtube_id, null: false, index: true
      
      t.string :name, null: true      
      t.string :link, null: true
      t.string :thumbnail, null: true
      
      t.integer :length, null: true
      t.integer :views, null: true

      t.timestamp :uploaded_at, null: true
      
      t.timestamps
    end
  end
end
