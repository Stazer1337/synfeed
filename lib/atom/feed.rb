class Atom::Feed
  attr_accessor :id, :title, :updated, # Required
                :author, :link, # Recommended
                :category, :contributor, :generator, :icon, :logo, :rights, :subtitle, # Optional
                :optional

  def initialize id, title, updated,
                 author = nil, link = nil,
                 category = nil, contributor = nil, generator = nil, icon = nil, logo = nil, rights = nil, subtitle = nil,
                 optional = nil
    @id = id
    @title = title
    @updated = updated
    
    @author = author
    @link = link

    @category = category
    @contributor = contributor
    @generator = generator
    @icon = icon
    @logo = logo
    @rights = rights
    @subtitle = subtitle

    @optional = optional
  end

  def self.from_attributes attributes
    Atom::Feed.new attributes[:id], attributes[:title], attributes[:updated],
                   attributes[:author], attributes[:link],
                   attributes[:category], attributes[:contributor], attributes[:generator], attributes[:icon], attributes[:logo], attributes[:rights], attributes[:subtitle],
                   attributes[:optional]
  end
end
