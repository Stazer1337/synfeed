# https://validator.w3.org/feed/docs/atom.html#text
class Atom::Text
  attr_accessor :text, :type
  
  def initialize text, type = 'text'
    @text = text
    @type = type
  end
end
