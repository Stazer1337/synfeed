class CreateChannels < ActiveRecord::Migration
  def change
    create_table :channels do |t|
      t.string :name, null: false, index: true
      t.string :password_digest, null: false

      t.timestamps
    end
  end
end
