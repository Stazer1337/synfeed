# https://validator.w3.org/feed/docs/atom.html#category
class Atom::Category
  attr_accessor :term, :scheme, :label
  
  def initialize term, scheme = nil, label = nil
    @term = term
    @scheme = scheme
    @label = label
  end
end
