# https://validator.w3.org/feed/docs/atom.html#link
class Atom::Link
  attr_reader :href, :rel, :type, :hreflang, :title, :length

  def initialize href, rel = nil, type = nil, hreflang = nil, title = nil, length = nil
    @href = href
    @rel = rel
    @type = type
    @hreflang = hreflang
    @title = title
    @length = length
  end
end
