# https://validator.w3.org/feed/docs/atom.html#person
class Atom::Person
  attr_accessor :name, :uri, :email
  
  def initialize name, uri = nil, email = nil
    @name = name
    @uri = uri
    @email = email
  end

  def to_h
    
  end
end
