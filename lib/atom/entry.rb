class Atom::Entry
  attr_accessor :id, :title, :updated, # Required
                :author, :content, :link, :summary, # Recommended
                :category, :contributor, :published, :rights, :source, # Optional
                :optional
                
  def initialize id, title, updated, author = nil, content = nil, link = nil, summary = nil, category = nil, contributor = nil, published = nil, rights = nil, source = nil, optional = nil
    @id = id
    @title = title
    @updated = updated
    
    @author = author
    @content = content
    @link = link
    @summary = summary

    @category = category
    @contributor = contributor
    @published = published
    @rights = rights
    @source = source

    @optional = optional
  end

  def self.from_attributes attributes
    Atom::Entry.new attributes[:id], attributes[:title], attributes[:updated],
                    attributes[:author], attributes[:content], attributes[:link], attributes[:summary],
                    attributes[:category], attributes[:contributor], attributes[:published], attributes[:rights], attributes[:source],
                    attributes[:optional]
  end
end
