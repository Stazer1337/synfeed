# https://validator.w3.org/feed/docs/atom.html#content
class Atom::Content
  attr_reader :content, :attributes

  def initialize content, attributes = nil
    @content = content
    @attributes = attributes
  end
end
