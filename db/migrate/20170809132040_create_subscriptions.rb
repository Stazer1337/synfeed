class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.references :channel, null: false, index: true
      t.references :feed, null: false, index: true

      t.timestamps
    end
  end
end
