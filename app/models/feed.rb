class Feed < ActiveRecord::Base
  # Relationships
  has_many :subscriptions
  has_many :channels, through: :subscriptions

  # Serialization
  serialize :miscellaneous, JSON
  
  # Refreshs feed content
  def refresh options = {}
    time = (options.delete(:time) || Time.now).to_i
    self.refresh! options if time >= self.refresh_rate.to_i + self.updated_at.to_i
  end

  # Abstract method for refreshing feed content
  def refresh! options = {}
    raise NotImplementedError
  end

  # Abstract method for picking(creating or selecting) a feed
  def pick options = {}
    raise NotImplementedError
  end

  # Abtract method for returning entries
  def entries
    raise NotImplementedError
  end
  
  # Abstract method which returns the data as atom feed
  def to_atom
    raise NotImplementedError
  end
end
