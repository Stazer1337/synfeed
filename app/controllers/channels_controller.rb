class ChannelsController < ApplicationController
  before_action :require_channel
  skip_before_action :require_channel, only: [:new, :create]

  def new
  end
  
  def create
    channel = Channel.new params.require(:channel).permit(:password, :password_confirmation)

    if !channel.save
      @errors = channel.errors
      render 'new'
    else
      unlock_channel channel
      redirect_to channel_path(channel)
    end
  end

  def show
    respond_to do |format|
      format.rss {render layout: false}
      format.html {}
    end
  end
    
  def clone
    if params.include? :channel
      channel = Channel.new params.require(:channel).permit(:password, :password_confirmation)
      channel.add_subscriptions @channel.subscriptions

      if !channel.save
        @errors = channel.errors
      else
        unlock_channel channel
        redirect_to channel_path(channel)
      end
    end
  end

  def unlock
    if params.include? :channel
      if !@channel.verify_password params.require(:channel).permit(:password)[:password]
        @errors = @channel.errors
      else
        unlock_channel @channel
        redirect_to channel_path(@channel)
      end
    end
  end
end
