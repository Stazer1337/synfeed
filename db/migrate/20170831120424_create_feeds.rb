class CreateFeeds < ActiveRecord::Migration
  def change
    create_table :feeds do |t|
      t.string :type, null: false, index: true
      t.integer :refresh_rate, null: false, default: 0

      t.string :miscellaneous, null: true
      
      t.timestamps
    end
  end
end
